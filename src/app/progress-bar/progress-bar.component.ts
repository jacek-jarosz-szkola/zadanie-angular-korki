import { NgStyle } from '@angular/common';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent {


  disabledColor = "gray"

  private _color: string = this.disabledColor;
  @Input()
  set color(col: string) {
    this._color = col;
  }
  get color(): string { 
    return this._color;
  }

  private _currentColor: string = this.disabledColor;
  set currentColor(col: string) {
    this._currentColor = col;
    this.style.backgroundColor = col;
  }
  get currentColor(): string {
    return this._currentColor;
  }

  @Input()
  min: number = 0;

  @Input()
  max: number = 255;

  @Input() 
  set value(value: number) {
    if(value > this.max) {
      this.style.width = "100%";
      this.currentColor = this.disabledColor;
      return;
    }

    this.currentColor = this.color;
    this.style.width = (value / this.max)*100 + "%";
  }

  style = 
  {
    backgroundColor: this.color,
    width: (this.value / this.max)*100 + "%",
  }
}
