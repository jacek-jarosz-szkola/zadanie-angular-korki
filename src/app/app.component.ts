import { Component, OnInit } from '@angular/core';


interface RGBColor {
  red: number,
  green: number,
  blue: number,
}

interface Calculation {
  name: string,
  calculate: (val1: number, val2: number) => number;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //zadanie 1
  farby?: number;
  calculate(surface: number) {
    if(isNaN(surface)) {
      this.farby = undefined;
      return;
    }
    this.farby = Math.ceil(surface / 4);
  }

  //zadanie 2
  changeR(r: number) {
    this.color.red = r; 
  }

  changeG(g: number) {
    this.color.green = g;
  }

  changeB(b: number) {
    this.color.blue = b;
  }

  color: RGBColor  = {
    red: 0,
    green: 0,
    blue: 0,
  }

  btnColor = "blue";

  colorInvalid(): boolean {
    return ( this.color.blue > 255 || this.color.blue < 0 ||
      this.color.green > 255 || this.color.green < 0 ||
      this.color.red > 255 || this.color.red < 0
    );
  }

  changeColor() {
    if(this.colorInvalid()) return;

    this.btnColor = `rgb(${this.color.red}, ${this.color.green}, ${this.color.blue})`;
  }

  orders = [
    "cytryna",
    "liść",
    "banan",
  ]

  onOrder(idx: number) {
    this.order = `twoje zamówienie to cukierek - ${this.orders[idx - 1] ?? "inny"}`;
  }

  order?: string;

  validValue(value?: number) {
    value = value ?? NaN;
    return !isNaN(value) && isFinite(value);
  }

  result?: number;
  //zadanie 3
  calculations: Calculation[] = [
  {
   name: "Dodawanie",
   calculate(val1, val2) {
     return val1 + val2;
   },
  },

  {
   name: "Odejmowanie",
   calculate(val1, val2) {
     return val1 - val2;
   },
  },
  {
   name: "Dzielenie",
   calculate(val1, val2) {
     return val1 / val2;
   },
  },
  {
   name: "Mnożenie",
   calculate(val1, val2) {
     return val1 * val2;
   },
  },
  {
   name: "Potęgowanie",
   calculate(val1, val2) {
    if (val2 < 0) return NaN;
    return val1 ** val2;

   },
  },
];

}
